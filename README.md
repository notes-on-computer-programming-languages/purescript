# purescript

[purescript.org](https://purescript.org)

# Official documentation
* [purescript.org](https://www.purescript.org/)
* [*Getting Started with PureScript*
  ](https://github.com/purescript/documentation/blob/master/guides/Getting-Started.md)
* [*PureScript by Example*
  ](https://book.purescript.org/)
  2014-2017 Phil Freeman
  * https://leanpub.com/purescript/read
* https://github.com/purescript
* [*Differences from Haskell*
  ](https://github.com/purescript/documentation/blob/master/language/Differences-from-Haskell.md)

## Package management
* [*purescript/spago*](https://github.com/purescript/spago) [spago](https://www.npmjs.com/package/spago)
* [*PureScript package management explained*
  ](https://discourse.purescript.org/t/post-purescript-package-management-explained/615)
  2019-02 justinw

# Unofficial documentation
* [*PureScript*](https://en.m.wikipedia.org/wiki/PureScript)
  WikipediA
* [purescript tutorial
  ](https://www.google.com/search?q=purescript+tutorial)
* [*Learn X in Y minutes, Where X=purescript*
  ](https://learnxinyminutes.com/docs/purescript/)
  2020 Fredrik Dyrkell, Thimoteus
* [*PureScript-Cookbook*
  ](https://github.com/JordanMartinez/purescript-cookbook)
  (2022) JordanMartinez
---
* [*High-level overview of PureScript*
  ](https://dev.to/zelenya/high-level-overview-of-purescript-37lf)
  2023-10 Zelenya (DEV)
* [*Purescript: Haskell + Javascript*
  ](https://mmhaskell.com/purescript)
  (2022) Monday Morning Haskell
  * [*Purescript Part 3: Simple Web UI's*
    ](https://mmhaskell.com/purescript-3)
* [*Four reasons that PureScript is your best choice to build a server in 2020*
  ](https://dev.to/meeshkan/four-reasons-that-purescript-is-your-best-choice-to-build-a-server-in-2020-3137)
  2021-08 Mike Solomon for Meeshkan
* [*Functional Programming Made Easier*
  ](https://leanpub.com/fp-made-easier)
  A Step-by-Step Guide
  2021-06 Charles Scalfani
* [*Generate PureScript Data Types From Haskell Data Types*
  ](https://medium.com/swlh/generate-purescript-data-types-from-haskell-data-types-a8568254a37c)
  2021-01 Ong Yi Ren
* [*Functional Programming for the Web: Getting Started with PureScript*
  ](https://medium.com/@KevinBGreene/functional-programming-for-the-web-getting-started-with-purescript-7387f8888318)
  2020-02 Kevin B. Greene
* [*Build a Moving Box with Purescript — An Introduction to Purescript Tutorial*
  ](https://levelup.gitconnected.com/building-a-moving-box-with-purescript-ae1a490429ab)
  2019-11 Gabriel Crispino
* [*Strictness surprises in PureScript Lazy lists*
  ](https://www.schoolofhaskell.com/user/griba/lazy_lists_on_strict_purescript)
  2018-04
* [*Using purescript-routing with purescript-halogen*
  ](https://www.parsonsmatt.org/2015/10/22/purescript_router.html)
  2015-10..2016-06 parsonsmatt
* [*Elm vs PureScript I*
  ](https://www.parsonsmatt.org/2015/10/03/elm_vs_purescript.html)
  2015-10 parsonsmatt

# Libraries
## Indices
* [pursuit](https://pursuit.purescript.org)
* [*awesome-purescript*
  ](https://project-awesome.org/passy/awesome-purescript)
* [*purescript-ecosystem*
  ](https://github.com/xgrommx/purescript-ecosystem)
  (2020)

## Some libraries
### DOM
* Cookbook: [*FindDomElementJs*
  ](https://github.com/JordanMartinez/purescript-cookbook/tree/master/recipes/FindDomElementJs)
* [*purescript-web-dom*
  ](https://pursuit.purescript.org/packages/purescript-web-dom)
* [*purescript-web-html*
  ](https://pursuit.purescript.org/packages/purescript-web-html)
* [*Build a Moving Box with Purescript — An Introduction to Purescript Tutorial*
  ](https://levelup.gitconnected.com/building-a-moving-box-with-purescript-ae1a490429ab)
  2019-11 Gabriel Crispino

### ORM
* [*droplet*
  ](https://github.com/easafe/purescript-droplet)
  Type-safe ORM for PureScript

### SQL
* [*selda*
  ](https://github.com/Kamirus/purescript-selda)

### UI
#### Halogen
* [*Halogen*
  ](https://github.com/purescript-halogen/purescript-halogen)
* [*Halogen is better than React at everything*
  ](https://chrisdone.com/posts/halogen-is-better-than-react/)
#### purescript-thermite

### Other libraries
* enums
* lazy
* lists

# Compiler
* [*Alternate backends*
  ](https://github.com/purescript/documentation/blob/master/ecosystem/Alternate-backends.md)

## PureScript native
* [*A native compiler backend for PureScript (via C++ or Golang)*
  ](https://github.com/andyarvanitis/purescript-native)
* [purescript to c++
  ](https://google.com/search?q=purescript+to+c%2B%2B)

